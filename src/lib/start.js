const builder = require('botbuilder');
const Student = require('../models/student');

builder.dialog('whatsWrongDialog', [
  (session, result)=> {
    builder.Prompt.choices('What is wrong?', ['I am in pain', 'I need to take my medicine for emergency', 'I am bleeding'])
  },
  (session, result) => {
    if (result.response === 'I am in pain'){
      //present a hero card of where the pain is located
    }

    if(result.response ==='I need to take my medicine'){
      //do calculation on time of last visit and in take of medicine from the lastVisit object
    }

    if(result.response === 'I am bleeding'){
      //ask where is the bleeding

      //tell to apply pressure

      //move student up the cue.
    }
  }
]);

builder.dialog('emergencyDialog', [
  (session, results) => {
    builder.Prompts.text(session, 'What is your name?')
  },
  (session, results) => {

    if (result.response) {
      // check student in DB
      Student.find({ name.first: result.response }).then( student => {
        console.log('Found something in student DB', student);
        if (student.length === 1){
          /// if one student come back with information confirm with DOB


          builder.Prompts.text(session, 'What is wrong?'); // [array of choices]
        } else if (student.length > 0){
          /// if multiple students with same first name display photo id

        } else {
          builder.Prompts.text(session, `${result.response}, looks like you may be a new student or we dont have your record. That is fine. Tell me what is wrong?`) // [array of choices]
        }
      }).catch(err => {
        console.error('Something went wrong when searching for the student information', err);
      })

    }

  },
  (session, results) => {
      session.beginDialog('whatsWrongDialog');
  }
]);

module.exports = [
  (session, results)=> {
    builder.send('Hello! My name is Nurse Charm!');
    builder.Prompts.text(session, 'Do you have an emergency?')
  },
  ,
  (session, result)=> {
     if(result.response === 'Yes'){
      session.beginDialog('emergencyDialog');
     } else {
       builder.Prompts.choice(session, 'Why are you here?', ['I need to take my medicine', 'I am sad and need to talk to the nurse', 'I am here for a check up']) // [array of choices]
     }
  },
  (session, result)=> {

    if ( result.response === 'I am here for a check up'){

      Student.find({ name.first: result.response }).then( student => {
        console.log('Found something in student DB', student);
        if (student.length === 1){
          /// if one student come back with information confirm with DOB


          builder.Prompts.text(session, 'What is wrong?'); // [array of choices]
        } else if (student.length > 0){
          /// if multiple students with same first name display photo id

        } else {
          builder.Prompts.text(session, `${result.response}, looks like you may be a new student or we dont have your record. That is fine. Tell me what is wrong?`) // [array of choices]
        }
      }).catch(err => {
        console.error('Something went wrong when searching for the student information', err);
      })
    }
    // check for Student in DB
    // if in DB check last visit and ask if they are still in pain
    //if not in DB ask why they are here.
    if(result){
      session.endDialog(`Hello! ${result.response} Nice to meet you! I have to go! Good Bye!`)
    }
  },
  (session, result)=> {

  }
]
