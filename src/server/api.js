const express = require('express');
const path = require('path')
const bodyParser = require('body-parser');
const logger = require('../helpers/log-driver');
const service = express();

service.use(bodyParser.json());
service.get('/status', (req, res) => {
  logger.info('Contacting Nurse Charm...')
  res.status(200).send('Nurse Charm is OK!');
});

service.use('/', (req, res) => {
  res.status('Hitting the root')
});


exports = module.exports = service;
