const botBuilder = require('botbuilder');
const botTester = require('bot-tester');

const connector = new botTester.TestConnector({
  defaultAddress: botBuilder.IAddress
});

const testBot = (connector, dialogs)=> {
  const testNurseCharm = new botBuilder.UniversalBot(connector, { storage: new botBuilder.MemoryBotStorage() });
  for (path in dialogs) {
    testNurseCharm.dialog(path, dialogs[path]);
  }

  return testNurseCharm;
}

module.exports = {
  connector: connector,
  testBot: testBot,
  botTester: botTester
}
