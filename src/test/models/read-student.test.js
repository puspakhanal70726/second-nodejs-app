const assert = require('assert');
const Student = require('../../models/student');

describe('[STUDENT] Read a record', ()=> {
  let JaneDoe, JohnDoe, JenNoe, JoeDoe, JoeNoe;
  beforeEach((done) => {
     JaneDoe = new Student({ firstName: 'Jane', lastName: 'Doe' });
     JohnDoe = new Student({ firstName: 'John', lastName: 'Doe' });
     JenNoe = new Student({ firstName: 'Jen', lastName: 'Noe' });
     JoeDoe = new Student({ firstName: 'Joe', lastName: 'Doe' });
     JoeNoe = new Student({ firstName: 'Joe', lastName: 'Noe'})

    Promise.all([JaneDoe.save(), JohnDoe.save(), JenNoe.save(), JoeDoe.save(), JoeNoe.save()])
      .then(() => done());
  });

  it('Read all students with the last name NOE', (done) => {
    Student.find({lastName: 'Noe'}).then((student)=> {
      assert.equal(student.length, 2);
      done();
    });
  });

  it('Read all students with the first name JEN', (done)=> {
    Student.find({firstName: 'Jen'}).then((student)=> {
      assert(student[0]._id.toString() === JenNoe._id.toString());
      done();
    });
  });
});
