const assert = require('assert');
const Student = require('../../models/student');

describe('[STUDENT] Update a record', ()=> {
  beforeEach((done) => {
    const JaneDoe = new Student({ firstName: 'Jane', lastName: 'Doe' });
    const JohnDoe = new Student({ firstName: 'John', lastName: 'Doe' });
    const JenNoe = new Student({ firstName: 'Jen', lastName: 'Noe' });
    const JoeDoe = new Student({ firstName: 'Joe', lastName: 'Doe' });
    const JoeNoe = new Student({ firstName: 'Joe', lastName: 'Noe'})

    Promise.all([JaneDoe.save(), JohnDoe.save(), JenNoe.save(), JoeDoe.save(), JoeNoe.save()])
      .then(() => done());
  });

  // it()
});
