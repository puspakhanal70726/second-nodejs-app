
## General
App is a chatbot dedicated to handling those tasks and automate the record keeping process on student health records.

## Dependencies
- node.js (v.10.0.0)
- MongoDB

## Getting Started
Microsoft Azure and uses their chatbot framework. To run it locally follow the steps below:
  1. Run `npm install`
  2. Start up MongoDB `brew services start mongodb` (Brew Package Manager)

## References and Publication
**Tutorials**
- [Building a Test Driven Chatbot for the Microsoft Bot Framework in Node JS](https://chatbotslife.com/building-a-test-driven-chatbot-for-the-microsoft-bot-framework-in-node-js-6374eeba7ffb)
